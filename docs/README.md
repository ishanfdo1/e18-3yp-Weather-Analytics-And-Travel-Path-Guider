---
layout: home
permalink: index.html

# Please update this with your repository name and project title
repository-name: e18-3yp-Weather-Analytics-And-Travel-Path-Guider
title: Weather Analytics and Travel Path Guider
---

[comment]: # "This is the standard layout for the project, but you can clean this and use your own template"

# Weather Analytics and Travel Path Guider

---

## Team

- E/18/098, Fernando K.A.I. , [e18098@eng.pdn.ac.lk](mailto:e18098@eng.pdn.ac.lk)
- E/18/100, Fernando K.N.A. , [e18100@eng.pdn.ac.lk](mailto:e18100@eng.pdn.ac.lk)
- E/18/155, Jayasundara J.W.K.R.B. , [e18155@eng.pdn.ac.lk](mailto:e18155@eng.pdn.ac.lk)

<!-- Image (photo/drawing of the final hardware) should be here -->

<!-- This is a sample image, to show how to add images to your page. To learn more options, please refer [this](https://projects.ce.pdn.ac.lk/docs/faq/how-to-add-an-image/) -->

![Sample Image](./images/sample.png)

#### Table of Contents

1. [Introduction](#introduction)
2. [Solution Architecture](#solution-architecture)
3. [Hardware & Software Designs](#hardware-and-software-designs)
4. [Testing](#testing)
5. [Detailed budget](#detailed-budget)
6. [Conclusion](#conclusion)
7. [Links](#links)

## Introduction

Description of the real world problem and solution, impact

## Solution Architecture

High level diagram + description

## Hardware and Software Designs

Detailed designs with many sub-sections

## Testing

Testing done on hardware and software, detailed + summarized results

## Detailed budget

All items and costs

| Item        | Quantity | Unit Cost |  Total |
| ----------- | :------: | :-------: | -----: |
| Sample item |    5     |  10 LKR   | 50 LKR |

## Conclusion

What was achieved, future developments, commercialization plans

## Links

- [Project Repository](https://github.com/cepdnaclk/{{ page.repository-name }}){:target="\_blank"}
- [Project Page](https://cepdnaclk.github.io/{{ page.repository-name}}){:target="\_blank"}
- [Department of Computer Engineering](http://www.ce.pdn.ac.lk/)
- [University of Peradeniya](https://eng.pdn.ac.lk/)

[//]: # "Please refer this to learn more about Markdown syntax"
[//]: # "https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet"
